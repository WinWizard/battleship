package bw_ist402.dylan.battleship.BattleShip;

import android.content.ClipData;
import android.content.ClipDescription;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

import bw_ist402.dylan.battleship.common.logger.Log;

public class BottomScreenFragment extends Fragment {
    Bundle savedState;

    ArrayList<ImageView> grid = new ArrayList(100);
    ArrayList<ImageView> ships = new ArrayList(5);

    int canPlaceShips; //cannot place when when value = 5
    int[] shipHits = new int[5];

    Button orientationButton;
    Button switchButton;
    Button forfeitButton;

    private TileListener dListener;
    private BtnListener bListener;

    public BottomScreenFragment() {
        super();
    }

    public static BottomScreenFragment newInstance() {
        BottomScreenFragment fragment = new BottomScreenFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bottom_screen,
                container, false);
        initInstances(rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Restore State Here
        if (!restoreStateFromArguments()) {
            dListener = new TileListener();
            // First Time running, Initialize something here
            for (ImageView view : grid) {
                view.setTag("empty");
                view.setOnDragListener(dListener);
            }
            canPlaceShips = 0;
            shipHits[0] = 5;
            shipHits[1] = 4;
            shipHits[2] = 3;
            shipHits[3] = 2;
            shipHits[4] = 3;
        }
    }

    private void initInstances(View v) {
        // init instance with v.findViewById here
        grid.clear();
        grid.add((ImageView) v.findViewById(R.id.A1));
        grid.add((ImageView) v.findViewById(R.id.A2));
        grid.add((ImageView) v.findViewById(R.id.A3));
        grid.add((ImageView) v.findViewById(R.id.A4));
        grid.add((ImageView) v.findViewById(R.id.A5));
        grid.add((ImageView) v.findViewById(R.id.A6));
        grid.add((ImageView) v.findViewById(R.id.A7));
        grid.add((ImageView) v.findViewById(R.id.A8));
        grid.add((ImageView) v.findViewById(R.id.A9));
        grid.add((ImageView) v.findViewById(R.id.A10));

        grid.add((ImageView) v.findViewById(R.id.B1));
        grid.add((ImageView) v.findViewById(R.id.B2));
        grid.add((ImageView) v.findViewById(R.id.B3));
        grid.add((ImageView) v.findViewById(R.id.B4));
        grid.add((ImageView) v.findViewById(R.id.B5));
        grid.add((ImageView) v.findViewById(R.id.B6));
        grid.add((ImageView) v.findViewById(R.id.B7));
        grid.add((ImageView) v.findViewById(R.id.B8));
        grid.add((ImageView) v.findViewById(R.id.B9));
        grid.add((ImageView) v.findViewById(R.id.B10));

        grid.add((ImageView) v.findViewById(R.id.C1));
        grid.add((ImageView) v.findViewById(R.id.C2));
        grid.add((ImageView) v.findViewById(R.id.C3));
        grid.add((ImageView) v.findViewById(R.id.C4));
        grid.add((ImageView) v.findViewById(R.id.C5));
        grid.add((ImageView) v.findViewById(R.id.C6));
        grid.add((ImageView) v.findViewById(R.id.C7));
        grid.add((ImageView) v.findViewById(R.id.C8));
        grid.add((ImageView) v.findViewById(R.id.C9));
        grid.add((ImageView) v.findViewById(R.id.C10));

        grid.add((ImageView) v.findViewById(R.id.D1));
        grid.add((ImageView) v.findViewById(R.id.D2));
        grid.add((ImageView) v.findViewById(R.id.D3));
        grid.add((ImageView) v.findViewById(R.id.D4));
        grid.add((ImageView) v.findViewById(R.id.D5));
        grid.add((ImageView) v.findViewById(R.id.D6));
        grid.add((ImageView) v.findViewById(R.id.D7));
        grid.add((ImageView) v.findViewById(R.id.D8));
        grid.add((ImageView) v.findViewById(R.id.D9));
        grid.add((ImageView) v.findViewById(R.id.D10));

        grid.add((ImageView) v.findViewById(R.id.E1));
        grid.add((ImageView) v.findViewById(R.id.E2));
        grid.add((ImageView) v.findViewById(R.id.E3));
        grid.add((ImageView) v.findViewById(R.id.E4));
        grid.add((ImageView) v.findViewById(R.id.E5));
        grid.add((ImageView) v.findViewById(R.id.E6));
        grid.add((ImageView) v.findViewById(R.id.E7));
        grid.add((ImageView) v.findViewById(R.id.E8));
        grid.add((ImageView) v.findViewById(R.id.E9));
        grid.add((ImageView) v.findViewById(R.id.E10));

        grid.add((ImageView) v.findViewById(R.id.F1));
        grid.add((ImageView) v.findViewById(R.id.F2));
        grid.add((ImageView) v.findViewById(R.id.F3));
        grid.add((ImageView) v.findViewById(R.id.F4));
        grid.add((ImageView) v.findViewById(R.id.F5));
        grid.add((ImageView) v.findViewById(R.id.F6));
        grid.add((ImageView) v.findViewById(R.id.F7));
        grid.add((ImageView) v.findViewById(R.id.F8));
        grid.add((ImageView) v.findViewById(R.id.F9));
        grid.add((ImageView) v.findViewById(R.id.F10));

        grid.add((ImageView) v.findViewById(R.id.G1));
        grid.add((ImageView) v.findViewById(R.id.G2));
        grid.add((ImageView) v.findViewById(R.id.G3));
        grid.add((ImageView) v.findViewById(R.id.G4));
        grid.add((ImageView) v.findViewById(R.id.G5));
        grid.add((ImageView) v.findViewById(R.id.G6));
        grid.add((ImageView) v.findViewById(R.id.G7));
        grid.add((ImageView) v.findViewById(R.id.G8));
        grid.add((ImageView) v.findViewById(R.id.G9));
        grid.add((ImageView) v.findViewById(R.id.G10));

        grid.add((ImageView) v.findViewById(R.id.H1));
        grid.add((ImageView) v.findViewById(R.id.H2));
        grid.add((ImageView) v.findViewById(R.id.H3));
        grid.add((ImageView) v.findViewById(R.id.H4));
        grid.add((ImageView) v.findViewById(R.id.H5));
        grid.add((ImageView) v.findViewById(R.id.H6));
        grid.add((ImageView) v.findViewById(R.id.H7));
        grid.add((ImageView) v.findViewById(R.id.H8));
        grid.add((ImageView) v.findViewById(R.id.H9));
        grid.add((ImageView) v.findViewById(R.id.H10));

        grid.add((ImageView) v.findViewById(R.id.I1));
        grid.add((ImageView) v.findViewById(R.id.I2));
        grid.add((ImageView) v.findViewById(R.id.I3));
        grid.add((ImageView) v.findViewById(R.id.I4));
        grid.add((ImageView) v.findViewById(R.id.I5));
        grid.add((ImageView) v.findViewById(R.id.I6));
        grid.add((ImageView) v.findViewById(R.id.I7));
        grid.add((ImageView) v.findViewById(R.id.I8));
        grid.add((ImageView) v.findViewById(R.id.I9));
        grid.add((ImageView) v.findViewById(R.id.I10));

        grid.add((ImageView) v.findViewById(R.id.J1));
        grid.add((ImageView) v.findViewById(R.id.J2));
        grid.add((ImageView) v.findViewById(R.id.J3));
        grid.add((ImageView) v.findViewById(R.id.J4));
        grid.add((ImageView) v.findViewById(R.id.J5));
        grid.add((ImageView) v.findViewById(R.id.J6));
        grid.add((ImageView) v.findViewById(R.id.J7));
        grid.add((ImageView) v.findViewById(R.id.J8));
        grid.add((ImageView) v.findViewById(R.id.J9));
        grid.add((ImageView) v.findViewById(R.id.J10));

        ships.clear();
        ships.add((ImageView) v.findViewById((R.id.Aircraft)));
        ships.add((ImageView) v.findViewById((R.id.Battleship)));
        ships.add((ImageView) v.findViewById((R.id.Destroyer)));
        ships.add((ImageView) v.findViewById((R.id.Patrol)));
        ships.add((ImageView) v.findViewById((R.id.Submarine)));

        //set up ships and drag listeners
        ships.get(0).setTag("Aircraft");
        ships.get(0).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (ships.get(0).getTag().equals("Aircraft")) {
                    ClipData.Item item = new ClipData.Item(v.getTag().toString());
                    ClipData dragData = new ClipData(v.getTag().toString(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);
                    View.DragShadowBuilder myShadow;
                    if (orientationButton.getText().equals("Vertical")) {
                        myShadow = new DragShadow(ships.get(0));
                    } else {
                        myShadow = new RotatedDragShadow(ships.get(0));

                    }

                    // Starts the drag

                    v.startDrag(dragData, myShadow, null, 0);
                    return true;
                }
                return false;
            }
        });

        ships.get(1).setTag("Battleship");
        ships.get(1).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (ships.get(1).getTag().equals("Battleship")) {
                    ClipData.Item item = new ClipData.Item(v.getTag().toString());
                    ClipData dragData = new ClipData(v.getTag().toString(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);
                    View.DragShadowBuilder myShadow;
                    if (orientationButton.getText().equals("Vertical")) {
                        myShadow = new DragShadow(ships.get(1));
                    } else {
                        myShadow = new RotatedDragShadow(ships.get(1));

                    }
                    // Starts the drag

                    v.startDrag(dragData, myShadow, null, 0);
                    return true;
                }
                return false;
            }
        });

        ships.get(2).setTag("Destroyer");
        ships.get(2).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (ships.get(2).getTag().equals("Destroyer")) {
                    ClipData.Item item = new ClipData.Item(v.getTag().toString());
                    ClipData dragData = new ClipData(v.getTag().toString(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);
                    View.DragShadowBuilder myShadow;
                    if (orientationButton.getText().equals("Vertical")) {
                        myShadow = new DragShadow(ships.get(2));
                    } else {
                        myShadow = new RotatedDragShadow(ships.get(2));

                    }

                    // Starts the drag

                    v.startDrag(dragData, myShadow, null, 0);
                    return true;
                }
                return false;
            }
        });

        ships.get(3).setTag("Patrol");
        ships.get(3).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (ships.get(3).getTag().equals("Patrol")) {
                    ClipData.Item item = new ClipData.Item(v.getTag().toString());
                    ClipData dragData = new ClipData(v.getTag().toString(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);
                    View.DragShadowBuilder myShadow;
                    if (orientationButton.getText().equals("Vertical")) {
                        myShadow = new DragShadow(ships.get(3));
                    } else {
                        myShadow = new RotatedDragShadow(ships.get(3));

                    }
                    // Starts the drag

                    v.startDrag(dragData, myShadow, null, 0);
                    return true;
                }
                return false;
            }
        });

        ships.get(4).setTag("Submarine");
        ships.get(4).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (ships.get(4).getTag().equals("Submarine")) {
                    ClipData.Item item = new ClipData.Item(v.getTag().toString());
                    ClipData dragData = new ClipData(v.getTag().toString(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);

                    View.DragShadowBuilder myShadow;
                    if (orientationButton.getText().equals("Vertical")) {
                        myShadow = new DragShadow(ships.get(4));
                    } else {
                        myShadow = new RotatedDragShadow(ships.get(4));

                    }
                    // Starts the drag

                    v.startDrag(dragData, myShadow, null, 0);
                    return true;
                }
                return false;
            }
        });

        //set up buttons and the listener for the button
        bListener = new BtnListener();

        orientationButton = (Button) v.findViewById(R.id.OrientationButton);
        orientationButton.setText("Vertical");
        orientationButton.setTag("Vertical");
        orientationButton.setOnClickListener(bListener);

        switchButton = (Button) v.findViewById(R.id.SwitchButton);
        switchButton.setOnClickListener(bListener);

        forfeitButton = (Button) v.findViewById(R.id.ForfeitButton);
        forfeitButton.setOnClickListener(bListener);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save State Here
        saveStateToArguments();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Save State Here
        saveStateToArguments();
    }

    ////////////////////
    // Donft Touch !!
    ////////////////////

    private void saveStateToArguments() {
        if (getView() != null)
            savedState = saveState();
        if (savedState != null) {
            Bundle b = getArguments();

            b.putBundle("saved", savedState);
        }
    }

    ////////////////////
    // Donft Touch !!
    ////////////////////

    private boolean restoreStateFromArguments() {
        Bundle b = getArguments();
        savedState = b.getBundle("saved");
        if (savedState != null) {
            restoreState();
            return true;
        }
        return false;
    }

    /////////////////////////////////
    // Restore Instance State Here
    /////////////////////////////////

    private void restoreState() {
        if (savedState != null) {
            SalvoActivity activity = (SalvoActivity) getActivity();

            canPlaceShips = savedState.getInt("pShips");

            shipHits = savedState.getIntArray("shipHits");

            ArrayList<String> tags = savedState.getStringArrayList("GTAGS");
            int i = 0;
            for (ImageView view : grid) {
                view.setTag(tags.get(i));
                view.setBackgroundResource(activity.getImg(tags.get(i)));

                i++;
            }

            ArrayList<String> stags = savedState.getStringArrayList("STAGS");
            i = 0;
            for (ImageView view : ships) {
                view.setTag(stags.get(i));
                view.setBackgroundResource(activity.getImg(stags.get(i)));

                i++;
            }
        }
    }

    //////////////////////////////
    // Save Instance State Here
    //////////////////////////////

    private Bundle saveState() {
        Bundle state = new Bundle();

        ArrayList<String> tags = new ArrayList<>();
        for (ImageView view : grid) {
            tags.add((String) view.getTag());
        }

        state.putStringArrayList("GTAGS", tags);

        ArrayList<String> stags = new ArrayList<>();
        for (ImageView view : ships) {
            stags.add((String) view.getTag());
        }

        state.putStringArrayList("STAGS", stags);

        state.putInt("pShips", canPlaceShips);

        state.putIntArray("shipHits", shipHits);

        return state;
    }

    //where the message received goes to find out what happens, and returns the result to the other player
    public void ping(int loc) {
        SalvoActivity activity = (SalvoActivity) getActivity();
        if (activity == null) {
            return;
        }

        //miss
        if (grid.get(loc).getTag().equals("empty")) {
            grid.get(loc).setTag("miss");
            grid.get(loc).setBackgroundResource(R.drawable.miss);
            activity.sendMsg("BM");
        }

        //hit
        else {
            String msg = "BH";
            grid.get(loc).setTag(grid.get(loc).getTag() + "hit");
            grid.get(loc).setBackgroundResource(activity.getImg("" + grid.get(loc).getTag()));
            switch (grid.get(loc).getTag().toString().charAt(0)) {
                case 'a':
                    shipHits[0]--;
                    //if last hit, move to sideboard
                    if (shipHits[0] == 0) {
                        msg += "A";
                        ships.get(0).setBackgroundResource(activity.getImg("aircraft_carrier_v"));
                        ships.get(0).setTag("Aircraft");
                    }
                    break;
                case 'b':
                    shipHits[1]--;
                    //if last hit, move to sideboard
                    if (shipHits[1] == 0) {
                        msg += "B";
                        ships.get(1).setBackgroundResource(activity.getImg("battleship_v"));
                        ships.get(1).setTag("Battleship");
                    }
                    break;
                case 'd':
                    shipHits[2]--;
                    //if last hit, move to sideboard
                    if (shipHits[2] == 0) {
                        msg += "D";
                        ships.get(2).setBackgroundResource(activity.getImg("destroyer_v"));
                        ships.get(2).setTag("Destroyer");
                    }
                    break;
                case 'p':
                    shipHits[3]--;
                    //if last hit, move to sideboard
                    if (shipHits[3] == 0) {
                        msg += "P";
                        ships.get(3).setBackgroundResource(activity.getImg("patrol_boat_v"));
                        ships.get(3).setTag("Patrol");
                    }
                    break;
                case 's':
                    shipHits[4]--;
                    //if last hit, move to sideboard
                    if (shipHits[4] == 0) {
                        msg += "S";
                        ships.get(4).setBackgroundResource(activity.getImg("submarine_v"));
                        ships.get(4).setTag("Submarine");
                    }
                    break;
            }
            activity.sendMsg(msg);
            //check to lose condition whenever a ship is hit
            boolean hasLost = true;
            for (int i = 0; i < 5; i++) {
                if (shipHits[i] != 0) {
                    hasLost = false;
                }
            }
            //call the lose condition
            if (hasLost) {
                activity.iLost();
            }
        }
    }


    private void switchToTop() {
        SalvoActivity activity = (SalvoActivity) getActivity();
        if (activity != null) {
            GameWindowFragment frag = activity.getGameWindow();
            frag.switchWindow();
        }
    }

    public void variableToast(String message, int duration) {
        final Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, duration);
    }

    //button listener
    private class BtnListener implements View.OnClickListener {
        @Override
        // On-click event handler -- method definition
        public void onClick(View view) {
            SalvoActivity activity = (SalvoActivity) getActivity();

            switch (view.getId()) {

                case R.id.OrientationButton:
                    if (orientationButton.getText().equals("Vertical")) {
                        orientationButton.setText("Horizontal");
                        orientationButton.setTag("Horizontal");
                    } else {
                        orientationButton.setText("Vertical");
                        orientationButton.setTag("Vertical");
                    }
                    break;
                case R.id.SwitchButton:
                    if (canPlaceShips == 5) {
                        switchToTop();
                    }
                    break;
                case R.id.ForfeitButton:
                    //end the game
                    activity.concede();
                    break;
                default:
                    variableToast("WTF I DON'T EVEN", 500);
                    //if
            }//switch
        }//OnClick
    }

    //grid listener
    public class TileListener implements View.OnDragListener {
        int length;
        boolean horizontal, valid;

        @Override
        public boolean onDrag(View v, DragEvent event) {
            final int action = event.getAction();

            // Handles each of the expected events
            switch (action) {

                case DragEvent.ACTION_DRAG_STARTED:

                    // Determines if this View can accept the dragged data
                    if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                        return true;
                    }

                    // Returns false. During the current drag and drop operation, this View will
                    // not receive events again until ACTION_DRAG_ENDED is sent.
                    return false;

                case DragEvent.ACTION_DRAG_ENTERED:
                    return true;

                case DragEvent.ACTION_DRAG_LOCATION:
                    return true;

                case DragEvent.ACTION_DRAG_EXITED:
                    return true;

                case DragEvent.ACTION_DROP:

                    String s = (String) event.getClipData().getDescription().getLabel();
                    if (s.equals("Aircraft")) {
                        length = 5;

                    } else if (s.equals("Battleship")) {
                        length = 4;

                    } else if (s.equals("Destroyer")) {
                        length = 3;

                    } else if (s.equals("Patrol")) {
                        length = 2;

                    } else if (s.equals("Submarine")) {
                        length = 3;

                    } else {
                        return false;
                    }

                    if (orientationButton.getText().equals("Horizontal")) {
                        horizontal = true;
                    } else {
                        horizontal = false;
                    }

                    valid = true;

                    if (horizontal) {
                        if (grid.indexOf(v) / 10 == (grid.indexOf(v) + length - 1) / 10) {
                            for (int i = 0; i < length; i++) {
                                if (!(grid.get(grid.indexOf(v) + i)).getTag().equals("empty")) {
                                    return false;
                                } else {

                                }
                            }
                            if (valid) {
                                if (s.equals("Aircraft")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.aircraft_carrier5);
                                    grid.get(grid.indexOf(v)).setTag("aircraft_carrier5");
                                    grid.get(grid.indexOf(v) + 1).setBackgroundResource(R.drawable.aircraft_carrier4);
                                    grid.get(grid.indexOf(v) + 1).setTag("aircraft_carrier4");
                                    grid.get(grid.indexOf(v) + 2).setBackgroundResource(R.drawable.aircraft_carrier3);
                                    grid.get(grid.indexOf(v) + 2).setTag("aircraft_carrier3");
                                    grid.get(grid.indexOf(v) + 3).setBackgroundResource(R.drawable.aircraft_carrier2);
                                    grid.get(grid.indexOf(v) + 3).setTag("aircraft_carrier2");
                                    grid.get(grid.indexOf(v) + 4).setBackgroundResource(R.drawable.aircraft_carrier1);
                                    grid.get(grid.indexOf(v) + 4).setTag("aircraft_carrier1");
                                    ships.get(0).setBackgroundResource(R.drawable.empty);
                                    ships.get(0).setTag("empty");
                                    canPlaceShips++;

                                } else if (s.equals("Battleship")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.battleship4);
                                    grid.get(grid.indexOf(v)).setTag("battleship4");
                                    grid.get(grid.indexOf(v) + 1).setBackgroundResource(R.drawable.battleship3);
                                    grid.get(grid.indexOf(v) + 1).setTag("battleship3");
                                    grid.get(grid.indexOf(v) + 2).setBackgroundResource(R.drawable.battleship2);
                                    grid.get(grid.indexOf(v) + 2).setTag("battleship2");
                                    grid.get(grid.indexOf(v) + 3).setBackgroundResource(R.drawable.battleship1);
                                    grid.get(grid.indexOf(v) + 3).setTag("battleship1");
                                    ships.get(1).setBackgroundResource(R.drawable.empty);
                                    ships.get(1).setTag("empty");
                                    canPlaceShips++;

                                } else if (s.equals("Destroyer")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.destroyer3);
                                    grid.get(grid.indexOf(v)).setTag("destroyer3");
                                    grid.get(grid.indexOf(v) + 1).setBackgroundResource(R.drawable.destroyer2);
                                    grid.get(grid.indexOf(v) + 1).setTag("destroyer2");
                                    grid.get(grid.indexOf(v) + 2).setBackgroundResource(R.drawable.destroyer1);
                                    grid.get(grid.indexOf(v) + 2).setTag("destroyer1");
                                    ships.get(2).setBackgroundResource(R.drawable.empty);
                                    ships.get(2).setTag("empty");
                                    canPlaceShips++;

                                } else if (s.equals("Patrol")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.patrol_boat2);
                                    grid.get(grid.indexOf(v)).setTag("patrol_boat2");
                                    grid.get(grid.indexOf(v) + 1).setBackgroundResource(R.drawable.patrol_boat1);
                                    grid.get(grid.indexOf(v) + 1).setTag("patrol_boat1");
                                    ships.get(3).setBackgroundResource(R.drawable.empty);
                                    ships.get(3).setTag("empty");
                                    canPlaceShips++;

                                } else if (s.equals("Submarine")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.submarine3);
                                    grid.get(grid.indexOf(v)).setTag("submarine3");
                                    grid.get(grid.indexOf(v) + 1).setBackgroundResource(R.drawable.submarine2);
                                    grid.get(grid.indexOf(v) + 1).setTag("submarine2");
                                    grid.get(grid.indexOf(v) + 2).setBackgroundResource(R.drawable.submarine1);
                                    grid.get(grid.indexOf(v) + 2).setTag("submarine1");
                                    ships.get(4).setBackgroundResource(R.drawable.empty);
                                    ships.get(4).setTag("empty");
                                    canPlaceShips++;

                                }

                                if (canPlaceShips == 5) {
                                    SalvoActivity activity = (SalvoActivity) getActivity();
                                    if (activity != null) {
                                        activity.sendMsg("Ready!");
                                    }
                                }

                                return true;
                            }
                        }
                    } else {
                        if (grid.indexOf(v) + (length - 1) * 10 < 100) {
                            for (int i = 0; i < length; i++) {
                                if (!(grid.get(grid.indexOf(v) + (10 * i))).getTag().equals("empty")) {
                                    return false;
                                }
                            }
                            if (valid) {
                                if (s.equals("Aircraft")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.aircraft_carrier1_v);
                                    grid.get(grid.indexOf(v)).setTag("aircraft_carrier1_v");
                                    grid.get(grid.indexOf(v) + 10).setBackgroundResource(R.drawable.aircraft_carrier2_v);
                                    grid.get(grid.indexOf(v) + 10).setTag("aircraft_carrier2_v");
                                    grid.get(grid.indexOf(v) + 20).setBackgroundResource(R.drawable.aircraft_carrier3_v);
                                    grid.get(grid.indexOf(v) + 20).setTag("aircraft_carrier3_v");
                                    grid.get(grid.indexOf(v) + 30).setBackgroundResource(R.drawable.aircraft_carrier4_v);
                                    grid.get(grid.indexOf(v) + 30).setTag("aircraft_carrier4_v");
                                    grid.get(grid.indexOf(v) + 40).setBackgroundResource(R.drawable.aircraft_carrier5_v);
                                    grid.get(grid.indexOf(v) + 40).setTag("aircraft_carrier5_v");
                                    ships.get(0).setBackgroundResource(R.drawable.empty);
                                    ships.get(0).setTag("empty");
                                    canPlaceShips++;

                                } else if (s.equals("Battleship")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.battleship1_v);
                                    grid.get(grid.indexOf(v)).setTag("battleship1_v");
                                    grid.get(grid.indexOf(v) + 10).setBackgroundResource(R.drawable.battleship2_v);
                                    grid.get(grid.indexOf(v) + 10).setTag("battleship2_v");
                                    grid.get(grid.indexOf(v) + 20).setBackgroundResource(R.drawable.battleship3_v);
                                    grid.get(grid.indexOf(v) + 20).setTag("battleship3_v");
                                    grid.get(grid.indexOf(v) + 30).setBackgroundResource(R.drawable.battleship4_v);
                                    grid.get(grid.indexOf(v) + 30).setTag("battleship4_v");
                                    ships.get(1).setBackgroundResource(R.drawable.empty);
                                    ships.get(1).setTag("empty");
                                    canPlaceShips++;

                                } else if (s.equals("Destroyer")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.destroyer1_v);
                                    grid.get(grid.indexOf(v)).setTag("destroyer1_v");
                                    grid.get(grid.indexOf(v) + 10).setBackgroundResource(R.drawable.destroyer2_v);
                                    grid.get(grid.indexOf(v) + 10).setTag("destroyer2_v");
                                    grid.get(grid.indexOf(v) + 20).setBackgroundResource(R.drawable.destroyer3_v);
                                    grid.get(grid.indexOf(v) + 20).setTag("destroyer3_v");
                                    ships.get(2).setBackgroundResource(R.drawable.empty);
                                    ships.get(2).setTag("empty");
                                    canPlaceShips++;

                                } else if (s.equals("Patrol")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.patrol_boat1_v);
                                    grid.get(grid.indexOf(v)).setTag("patrol_boat1_v");
                                    grid.get(grid.indexOf(v) + 10).setBackgroundResource(R.drawable.patrol_boat2_v);
                                    grid.get(grid.indexOf(v) + 10).setTag("patrol_boat2_v");
                                    ships.get(3).setBackgroundResource(R.drawable.empty);
                                    ships.get(3).setTag("empty");
                                    canPlaceShips++;

                                } else if (s.equals("Submarine")) {
                                    grid.get(grid.indexOf(v)).setBackgroundResource(R.drawable.submarine1_v);
                                    grid.get(grid.indexOf(v)).setTag("submarine1_v");
                                    grid.get(grid.indexOf(v) + 10).setBackgroundResource(R.drawable.submarine2_v);
                                    grid.get(grid.indexOf(v) + 10).setTag("submarine2_v");
                                    grid.get(grid.indexOf(v) + 20).setBackgroundResource(R.drawable.submarine3_v);
                                    grid.get(grid.indexOf(v) + 20).setTag("submarine3_v");
                                    ships.get(4).setBackgroundResource(R.drawable.empty);
                                    ships.get(4).setTag("empty");
                                    canPlaceShips++;

                                }
                                if (canPlaceShips == 5) {
                                    SalvoActivity activity = (SalvoActivity) getActivity();
                                    if (activity != null) {
                                        activity.sendMsg("Ready!");
                                    }
                                }
                                return true;
                            }
                        }
                    }

                    break;


                case DragEvent.ACTION_DRAG_ENDED:
                    return true;

                // An unknown action type was received.
                default:
                    Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                    break;
            }

            return false;
        }
    }

    //Drag shadow builders for both the horizontal and vertical orientations

    public class RotatedDragShadow extends View.DragShadowBuilder {
        final int width;
        final int height;
        View view;
        double rotationRad = Math.toRadians(-90);
        int w;
        int h;
        double s = Math.abs(Math.sin(rotationRad));
        double c = Math.abs(Math.cos(rotationRad));

        public RotatedDragShadow(View view) {
            super(view);

            this.view = view;
            w = (int) (view.getWidth() * view.getScaleX());
            h = (int) (view.getHeight() * view.getScaleY());
            width = (int) (w * c + h * s);
            height = (int) (w * s + h * c);
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            canvas.scale(view.getScaleX(), view.getScaleY(), width / 2,
                    height / 2);

            canvas.rotate(-90, width / 2, height / 2);
            canvas.translate((width - view.getWidth()) / 2,
                    (height - view.getHeight()) / 2);
            super.onDrawShadow(canvas);
        }

        @Override
        public void onProvideShadowMetrics(Point shadowSize,
                                           Point shadowTouchPoint) {
            shadowSize.set(width, height);
            shadowTouchPoint.set(shadowSize.x / 10, shadowSize.y / 2);
        }
    }

    public class DragShadow extends View.DragShadowBuilder {

        public DragShadow(View view) {
            super(view);
        }

        @Override
        public void onProvideShadowMetrics(Point shadowSize,
                                           Point shadowTouchPoint) {
            shadowSize.set(getView().getWidth(), getView().getHeight());
            shadowTouchPoint.set(shadowSize.x / 2, shadowSize.y / 10);
        }
    }
}