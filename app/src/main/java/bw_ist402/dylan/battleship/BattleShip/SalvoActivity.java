/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bw_ist402.dylan.battleship.BattleShip;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import bw_ist402.dylan.battleship.common.logger.Log;
import bw_ist402.dylan.battleship.common.logger.LogWrapper;
import bw_ist402.dylan.battleship.common.logger.MessageOnlyLogFilter;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
public class SalvoActivity extends FragmentActivity {

    private static final String TAG = "BattleShipFragment";
    /**
     * The action listener for the EditText widget, to listen for the return key
     */
    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    private static final int REQUEST_CONCEDE = 4;
    //Game Window
    protected GameWindowFragment window;
    private HashMap<String, Integer> imgMap;
    /**
     * String buffer for outgoing messages
     */
    private StringBuffer mOutStringBuffer;
    /**
     * Local Bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter = null;
    /**
     * Member object for the chat services
     */
    private BluetoothService dataService = null;
    private ArrayList<String> fTags;
    /**
     * Name of the connected device
     */
    private String mConnectedDeviceName = null;
    private String recieved = null;
    /**
     * The Handler that gets information back from the BluetoothService
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            View decorView;
            int uiOptions;
            ActionBar actionBar;
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:

                            setGameWindow(window);
                            decorView = getWindow().getDecorView();
// Hide the status bar.
                            uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                            decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
                            actionBar = getActionBar();
                            actionBar.hide();

                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));

                            break;
                        case BluetoothService.STATE_CONNECTING:
                            setStatus(R.string.title_connecting);
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            setStatus(R.string.title_not_connected);
                            decorView = getWindow().getDecorView();
// Hide the status bar.
                            uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
                            decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
                            actionBar = getActionBar();
                            actionBar.show();
                            setGameWindow(new NotConnectedFragment());
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    if (!window.isBScreen) {
                        window.toggleSwitch();
                        window.disableFire();
                    }
                    if (window.isBScreen && writeMessage.charAt(0) == 'B') {
                        window.toggleSwitch();
                        window.enableFire();
                    }

                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;

                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    // mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);

                    recieved = readMessage;
                    if (recieved.charAt(0) == 'F') {
                        String toastMsg = "";
                        if (recieved.length() == 2) {
                            toastMsg = "A" + (recieved.charAt(1) == '9' ? "10" : ((char) (recieved.charAt(1) + 1)));
                        } else {
                            switch (recieved.charAt(1)) {
                                case '1':
                                    toastMsg = "B" + (recieved.charAt(2) == '9' ? "10" : ((char) (recieved.charAt(2) + 1)));
                                    break;
                                case '2':
                                    toastMsg = "C" + (recieved.charAt(2) == '9' ? "10" : ((char) (recieved.charAt(2) + 1)));
                                    break;
                                case '3':
                                    toastMsg = "D" + (recieved.charAt(2) == '9' ? "10" : ((char) (recieved.charAt(2) + 1)));
                                    break;
                                case '4':
                                    toastMsg = "E" + (recieved.charAt(2) == '9' ? "10" : ((char) (recieved.charAt(2) + 1)));

                                    break;
                                case '5':
                                    toastMsg = "F" + (recieved.charAt(2) == '9' ? "10" : ((char) (recieved.charAt(2) + 1)));

                                    break;
                                case '6':
                                    toastMsg = "G" + (recieved.charAt(2) == '9' ? "10" : ((char) (recieved.charAt(2) + 1)));

                                    break;
                                case '7':
                                    toastMsg = "H" + (recieved.charAt(2) == '9' ? "10" : ((char) (recieved.charAt(2) + 1)));

                                    break;
                                case '8':
                                    toastMsg = "I" + (recieved.charAt(2) == '9' ? "10" : ((char) (recieved.charAt(2) + 1)));

                                    break;
                                case '9':
                                    toastMsg = "J" + (recieved.charAt(2) == '9' ? "10" : ((char) (recieved.charAt(2) + 1)));

                                    break;
                            }
                        }
                        variableToast(toastMsg, 1000);

                        if (!window.isBScreen) {
                            window.switchWindow();
                        }
                        window.toggleSwitch();
                        window.bScreen.ping(Integer.parseInt(recieved.substring(1)));
                    }
                    if (recieved.charAt(0) == 'B') {
                        switch (recieved.charAt(1)) {
                            case 'H':
                                variableToast("Hit!", 1000);
                                break;
                            case 'M':
                                variableToast("Miss!", 1000);
                                break;
                        }
                        if (recieved.contains("HA") || recieved.contains("HD") || recieved.contains("HP") || recieved.contains("HB") || recieved.contains("HS")) {
                            variableToast("You sunk my battleship!", 2000);
                        }
                        window.fScreen.ping(recieved.substring(1));
                        if (!window.isBScreen) {
                            window.toggleSwitch();
                        }
                    }
                    if (recieved.charAt(0) == 'R') {
                        variableToast(recieved, 1500);
                        window.enableFire();
                    }
                    if (recieved.charAt(0) == 'Y') {
                        variableToast(recieved, 1500);
                        iWon();
                    }
                    if (recieved.charAt(0) == 'D') {
                        sendMsg("AND YOU BUDDY");
                    }
                    Log.d("recieved", recieved);
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);

                    Toast.makeText(getApplicationContext(), "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();

                    break;
                case Constants.MESSAGE_TOAST:

                    Toast.makeText(getApplicationContext(), msg.getData().getString(Constants.TOAST), Toast.LENGTH_SHORT).show();

                    break;
            }
        }
    };



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instantiateImgMap();
        setContentView(R.layout.activity_battleship);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {

            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
        }
        if (savedInstanceState == null) {

            Fragment frag = new NotConnectedFragment();
            setGameWindow(frag);
            window = new GameWindowFragment();

        }



    }

    public int getImg(String img) {
        return imgMap.get(img);
    }

    private void instantiateImgMap() {
        imgMap = new HashMap<>();
        imgMap.put("empty", R.drawable.empty);
        imgMap.put("hit", R.drawable.hit);
        imgMap.put("miss", R.drawable.miss);
        imgMap.put("reticule", R.drawable.reticule);
        imgMap.put("aircraft_carrier", R.drawable.aircraft_carrier);
        imgMap.put("aircraft_carrier_v", R.drawable.aircraft_carrier_v);
        imgMap.put("aircraft_carrier1", R.drawable.aircraft_carrier1);
        imgMap.put("aircraft_carrier2", R.drawable.aircraft_carrier2);
        imgMap.put("aircraft_carrier3", R.drawable.aircraft_carrier3);
        imgMap.put("aircraft_carrier4", R.drawable.aircraft_carrier4);
        imgMap.put("aircraft_carrier5", R.drawable.aircraft_carrier5);
        imgMap.put("aircraft_carrier1_v", R.drawable.aircraft_carrier1_v);
        imgMap.put("aircraft_carrier2_v", R.drawable.aircraft_carrier2_v);
        imgMap.put("aircraft_carrier3_v", R.drawable.aircraft_carrier3_v);
        imgMap.put("aircraft_carrier4_v", R.drawable.aircraft_carrier4_v);
        imgMap.put("aircraft_carrier5_v", R.drawable.aircraft_carrier5_v);
        imgMap.put("aircraft_carrier1hit", R.drawable.aircraft_carrier1hit);
        imgMap.put("aircraft_carrier2hit", R.drawable.aircraft_carrier2hit);
        imgMap.put("aircraft_carrier3hit", R.drawable.aircraft_carrier3hit);
        imgMap.put("aircraft_carrier4hit", R.drawable.aircraft_carrier4hit);
        imgMap.put("aircraft_carrier5hit", R.drawable.aircraft_carrier5hit);
        imgMap.put("aircraft_carrier1_vhit", R.drawable.aircraft_carrier1_vhit);
        imgMap.put("aircraft_carrier2_vhit", R.drawable.aircraft_carrier2_vhit);
        imgMap.put("aircraft_carrier3_vhit", R.drawable.aircraft_carrier3_vhit);
        imgMap.put("aircraft_carrier4_vhit", R.drawable.aircraft_carrier4_vhit);
        imgMap.put("aircraft_carrier5_vhit", R.drawable.aircraft_carrier5_vhit);
        imgMap.put("battleship", R.drawable.battleship);
        imgMap.put("battleship_v", R.drawable.battleship_v);
        imgMap.put("battleship1", R.drawable.battleship1);
        imgMap.put("battleship2", R.drawable.battleship2);
        imgMap.put("battleship3", R.drawable.battleship3);
        imgMap.put("battleship4", R.drawable.battleship4);
        imgMap.put("battleship1_v", R.drawable.battleship1_v);
        imgMap.put("battleship2_v", R.drawable.battleship2_v);
        imgMap.put("battleship3_v", R.drawable.battleship3_v);
        imgMap.put("battleship4_v", R.drawable.battleship4_v);
        imgMap.put("battleship1hit", R.drawable.battleship1hit);
        imgMap.put("battleship2hit", R.drawable.battleship2hit);
        imgMap.put("battleship3hit", R.drawable.battleship3hit);
        imgMap.put("battleship4hit", R.drawable.battleship4hit);
        imgMap.put("battleship1_vhit", R.drawable.battleship1_vhit);
        imgMap.put("battleship2_vhit", R.drawable.battleship2_vhit);
        imgMap.put("battleship3_vhit", R.drawable.battleship3hit_v);
        imgMap.put("battleship4_vhit", R.drawable.battleship4_vhit);
        imgMap.put("destroyer", R.drawable.destroyer);
        imgMap.put("destroyer_v", R.drawable.destroyer_v);
        imgMap.put("destroyer1", R.drawable.destroyer1);
        imgMap.put("destroyer2", R.drawable.destroyer2);
        imgMap.put("destroyer3", R.drawable.destroyer3);
        imgMap.put("destroyer1_v", R.drawable.destroyer1_v);
        imgMap.put("destroyer2_v", R.drawable.destroyer2_v);
        imgMap.put("destroyer3_v", R.drawable.destroyer3_v);
        imgMap.put("destroyer1hit", R.drawable.destroyer1hit);
        imgMap.put("destroyer2hit", R.drawable.destroyer2hit);
        imgMap.put("destroyer3hit", R.drawable.destroyer3hit);
        imgMap.put("destroyer1_vhit", R.drawable.destroyer1_vhit);
        imgMap.put("destroyer2_vhit", R.drawable.destroyer2_vhit);
        imgMap.put("destroyer3_vhit", R.drawable.destroyer3_vhit);
        imgMap.put("submarine", R.drawable.submarine);
        imgMap.put("submarine_v", R.drawable.submarine_v);
        imgMap.put("submarine1", R.drawable.submarine1);
        imgMap.put("submarine2", R.drawable.submarine2);
        imgMap.put("submarine3", R.drawable.submarine3);
        imgMap.put("submarine1_v", R.drawable.submarine1_v);
        imgMap.put("submarine2_v", R.drawable.submarine2_v);
        imgMap.put("submarine3_v", R.drawable.submarine3_v);
        imgMap.put("submarine1hit", R.drawable.submarine1hit);
        imgMap.put("submarine2hit", R.drawable.submarine2hit);
        imgMap.put("submarine3hit", R.drawable.submarine3hit);
        imgMap.put("submarine1_vhit", R.drawable.submarine1_vhit);
        imgMap.put("submarine2_vhit", R.drawable.submarine2_vhit);
        imgMap.put("submarine3_vhit", R.drawable.submarine3_vhit);
        imgMap.put("patrol_boat", R.drawable.patrol_boat);
        imgMap.put("patrol_boat_v", R.drawable.patrol_boat_v);
        imgMap.put("patrol_boat1", R.drawable.patrol_boat1);
        imgMap.put("patrol_boat2", R.drawable.patrol_boat2);
        imgMap.put("patrol_boat1_v", R.drawable.patrol_boat1_v);
        imgMap.put("patrol_boat2_v", R.drawable.patrol_boat2_v);
        imgMap.put("patrol_boat1hit", R.drawable.patrol_boat1hit);
        imgMap.put("patrol_boat2hit", R.drawable.patrol_boat2hit);
        imgMap.put("patrol_boat1_vhit", R.drawable.patrol_boat1_vhit);
        imgMap.put("patrol_boat2_vhit", R.drawable.patrol_boat2_vhit);
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeLogging();
        // If BT is not on, request that it be enabled.
        // will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else if (dataService == null) {
            setupBluetooth();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dataService != null) {
            dataService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (dataService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (dataService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth chat services
                dataService.start();
            }
        }
    }

    /**
     * Set up the UI and background operations for chat.
     */
    private void setupBluetooth() {
        Log.d(TAG, "setupBluetooth()");

        // Initialize the BluetoothService to perform bluetooth connections
        dataService = new BluetoothService(this, mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");
    }

    /**
     * Makes this device discoverable.
     */
    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */

    public void sendMsg(String message) {
        // Check that we're actually connected before trying anything
        if (dataService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }


        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothService to write
            Log.d("message", "" + message);

            byte[] send = message.getBytes();
            dataService.write(send);

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
        }
    }


    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(int resId) {
        Activity activity = this;
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(resId);
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private void setStatus(CharSequence subTitle) {
        Activity activity = this;
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupBluetooth();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    this.finish();
                }
                break;
            case REQUEST_CONCEDE:
                if (resultCode == Activity.RESULT_OK) {
                    iLost();
                }

                break;
        }
    }

    /**
     * Establish connection with other device
     *
     * @param data   An {@link Intent} with {@link DeviceListActivity#EXTRA_DEVICE_ADDRESS} extra.
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        dataService.connect(device, secure);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.bluetooth_chat, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.secure_connect_scan: {
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                return true;
            }
            case R.id.insecure_connect_scan: {
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
                return true;
            }
            case R.id.discoverable: {
                // Ensure this device is discoverable by others
                ensureDiscoverable();
                return true;
            }
        }
        return false;
    }

    /**
     * Create a chain of targets that will receive log data
     */

    public void initializeLogging() {
        // Wraps Android's native log framework.
        LogWrapper logWrapper = new LogWrapper();
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        Log.setLogNode(logWrapper);

        // Filter strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        logWrapper.setNext(msgFilter);

        // On screen logging via a fragment with a TextView.

        Log.i(TAG, "Ready");
    }


    public void variableToast(String message, int duration) {
        final Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, duration);
    }

    public GameWindowFragment getGameWindow() {
        return window;
    }

    private synchronized void setGameWindow(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.contentFragment, fragment); //Container -> R.id.contentFragment
        transaction.commit();


    }

    public void concede() {
        Intent intent = new Intent(this, ConcedeActivity.class);
        int requestCode = REQUEST_CONCEDE; // Or some number you choose
        startActivityForResult(intent, requestCode);
    }
    public void iLost() {
        variableToast("You Lose!", 1500);
        sendMsg("You Won!");
    }

    public void iWon() {
        dataService.endGame();
        sendMsg("DIE");
    }


}
