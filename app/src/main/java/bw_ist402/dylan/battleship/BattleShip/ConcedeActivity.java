package bw_ist402.dylan.battleship.BattleShip;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class ConcedeActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(Activity.RESULT_CANCELED);
        setContentView(R.layout.activity_concede);

        BtnListener listener = new BtnListener();
        findViewById(R.id.yes).setOnClickListener(listener);
        findViewById(R.id.no).setOnClickListener(listener);

    }

    private class BtnListener implements View.OnClickListener {
        @Override
        // On-click event handler -- method definition
        public void onClick(View view) {
            Intent intent = new Intent();


            switch (view.getId()) {

                case R.id.yes:
                    intent.putExtra("someValue", "data");
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                case R.id.no:
                    intent.putExtra("someValue", "data");
                    setResult(RESULT_CANCELED, intent);
                    finish();
                    break;
                //if
            }//switch
        }//OnClick
    }


}
