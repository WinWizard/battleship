package bw_ist402.dylan.battleship.BattleShip;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class GameWindowFragment extends Fragment {
    protected boolean isBScreen;

    protected FireScreenFragment fScreen;
    protected BottomScreenFragment bScreen;

    public GameWindowFragment() {
        // Required empty public constructor
    }

    private void addGameWindow(boolean isVisible, int id) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (id == R.id.game1) {
            transaction.replace(id, fScreen); //Container -> R.id.contentFragment
            if (!isVisible)
                transaction.hide(fScreen);
        } else if (id == R.id.game) {
            transaction.replace(id, bScreen);
            if (!isVisible)
                transaction.hide(bScreen);
        }

        transaction.commit();
    }

    private synchronized void setGameWindow(/*Fragment fragment*/int id) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        if (id == R.id.game1) {
            transaction.hide(bScreen);
            transaction.show(fScreen);
        } else if (id == R.id.game) {
            transaction.hide(fScreen);
            transaction.show(bScreen);
        }
        transaction.commit();
    }

    public void enableSwitch() {
        if (isBScreen) {
            bScreen.switchButton.setEnabled(true);
        } else {
            fScreen.switchButton.setEnabled(true);
        }
    }

    public void disableSwitch() {
        if (isBScreen) {
            bScreen.switchButton.setEnabled(false);
        } else {
            fScreen.switchButton.setEnabled(false);
        }
    }

    @Deprecated //Replaced by disable and enable
    public void toggleSwitch() {
        if (isBScreen) {
            bScreen.switchButton.setEnabled(!bScreen.switchButton.isEnabled());
        } else {
            fScreen.switchButton.setEnabled(!fScreen.switchButton.isEnabled());
        }
    }

    public void disableFire() {
        fScreen.fireButton.setEnabled(false);
    }

    public void enableFire() {
        fScreen.fireButton.setEnabled(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bScreen = bScreen.newInstance();
        fScreen = fScreen.newInstance();
        isBScreen = true;

        addGameWindow(true, R.id.game);
        addGameWindow(false, R.id.game1);
    }

    public void switchWindow() {
        if (isBScreen) {
            setGameWindow(R.id.game1);
            isBScreen = false;
        } else {
            setGameWindow(R.id.game);
            isBScreen = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_window, container, false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
