package bw_ist402.dylan.battleship.BattleShip;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;


public class FireScreenFragment extends Fragment {

    Bundle savedState;
    Button switchButton;
    Button forfeitButton;
    Button fireButton;
    private ArrayList<ImageView> grid = new ArrayList(100);
    private View target = null;
    private BtnListener listener = new BtnListener();
    private View tempTarget;


    public FireScreenFragment() {
        super();
    }

    public static FireScreenFragment newInstance() {
        FireScreenFragment fragment = new FireScreenFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fire_screen,
                container, false);
        initInstances(rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Restore State Here

        if (!restoreStateFromArguments()) {
            for (ImageView view : grid) {
                view.setTag("empty");
                view.setOnClickListener(listener);
            }
        }
    }

    //populate the grid
    private void initInstances(View v) {
        // init instance with v.findViewById here
        grid.clear();
        grid.add((ImageButton) v.findViewById(R.id.A1));
        grid.add((ImageButton) v.findViewById(R.id.A2));
        grid.add((ImageButton) v.findViewById(R.id.A3));
        grid.add((ImageButton) v.findViewById(R.id.A4));
        grid.add((ImageButton) v.findViewById(R.id.A5));
        grid.add((ImageButton) v.findViewById(R.id.A6));
        grid.add((ImageButton) v.findViewById(R.id.A7));
        grid.add((ImageButton) v.findViewById(R.id.A8));
        grid.add((ImageButton) v.findViewById(R.id.A9));
        grid.add((ImageButton) v.findViewById(R.id.A10));

        grid.add((ImageButton) v.findViewById(R.id.B1));
        grid.add((ImageButton) v.findViewById(R.id.B2));
        grid.add((ImageButton) v.findViewById(R.id.B3));
        grid.add((ImageButton) v.findViewById(R.id.B4));
        grid.add((ImageButton) v.findViewById(R.id.B5));
        grid.add((ImageButton) v.findViewById(R.id.B6));
        grid.add((ImageButton) v.findViewById(R.id.B7));
        grid.add((ImageButton) v.findViewById(R.id.B8));
        grid.add((ImageButton) v.findViewById(R.id.B9));
        grid.add((ImageButton) v.findViewById(R.id.B10));

        grid.add((ImageButton) v.findViewById(R.id.C1));
        grid.add((ImageButton) v.findViewById(R.id.C2));
        grid.add((ImageButton) v.findViewById(R.id.C3));
        grid.add((ImageButton) v.findViewById(R.id.C4));
        grid.add((ImageButton) v.findViewById(R.id.C5));
        grid.add((ImageButton) v.findViewById(R.id.C6));
        grid.add((ImageButton) v.findViewById(R.id.C7));
        grid.add((ImageButton) v.findViewById(R.id.C8));
        grid.add((ImageButton) v.findViewById(R.id.C9));
        grid.add((ImageButton) v.findViewById(R.id.C10));

        grid.add((ImageButton) v.findViewById(R.id.D1));
        grid.add((ImageButton) v.findViewById(R.id.D2));
        grid.add((ImageButton) v.findViewById(R.id.D3));
        grid.add((ImageButton) v.findViewById(R.id.D4));
        grid.add((ImageButton) v.findViewById(R.id.D5));
        grid.add((ImageButton) v.findViewById(R.id.D6));
        grid.add((ImageButton) v.findViewById(R.id.D7));
        grid.add((ImageButton) v.findViewById(R.id.D8));
        grid.add((ImageButton) v.findViewById(R.id.D9));
        grid.add((ImageButton) v.findViewById(R.id.D10));

        grid.add((ImageButton) v.findViewById(R.id.E1));
        grid.add((ImageButton) v.findViewById(R.id.E2));
        grid.add((ImageButton) v.findViewById(R.id.E3));
        grid.add((ImageButton) v.findViewById(R.id.E4));
        grid.add((ImageButton) v.findViewById(R.id.E5));
        grid.add((ImageButton) v.findViewById(R.id.E6));
        grid.add((ImageButton) v.findViewById(R.id.E7));
        grid.add((ImageButton) v.findViewById(R.id.E8));
        grid.add((ImageButton) v.findViewById(R.id.E9));
        grid.add((ImageButton) v.findViewById(R.id.E10));

        grid.add((ImageButton) v.findViewById(R.id.F1));
        grid.add((ImageButton) v.findViewById(R.id.F2));
        grid.add((ImageButton) v.findViewById(R.id.F3));
        grid.add((ImageButton) v.findViewById(R.id.F4));
        grid.add((ImageButton) v.findViewById(R.id.F5));
        grid.add((ImageButton) v.findViewById(R.id.F6));
        grid.add((ImageButton) v.findViewById(R.id.F7));
        grid.add((ImageButton) v.findViewById(R.id.F8));
        grid.add((ImageButton) v.findViewById(R.id.F9));
        grid.add((ImageButton) v.findViewById(R.id.F10));

        grid.add((ImageButton) v.findViewById(R.id.G1));
        grid.add((ImageButton) v.findViewById(R.id.G2));
        grid.add((ImageButton) v.findViewById(R.id.G3));
        grid.add((ImageButton) v.findViewById(R.id.G4));
        grid.add((ImageButton) v.findViewById(R.id.G5));
        grid.add((ImageButton) v.findViewById(R.id.G6));
        grid.add((ImageButton) v.findViewById(R.id.G7));
        grid.add((ImageButton) v.findViewById(R.id.G8));
        grid.add((ImageButton) v.findViewById(R.id.G9));
        grid.add((ImageButton) v.findViewById(R.id.G10));

        grid.add((ImageButton) v.findViewById(R.id.H1));
        grid.add((ImageButton) v.findViewById(R.id.H2));
        grid.add((ImageButton) v.findViewById(R.id.H3));
        grid.add((ImageButton) v.findViewById(R.id.H4));
        grid.add((ImageButton) v.findViewById(R.id.H5));
        grid.add((ImageButton) v.findViewById(R.id.H6));
        grid.add((ImageButton) v.findViewById(R.id.H7));
        grid.add((ImageButton) v.findViewById(R.id.H8));
        grid.add((ImageButton) v.findViewById(R.id.H9));
        grid.add((ImageButton) v.findViewById(R.id.H10));

        grid.add((ImageButton) v.findViewById(R.id.I1));
        grid.add((ImageButton) v.findViewById(R.id.I2));
        grid.add((ImageButton) v.findViewById(R.id.I3));
        grid.add((ImageButton) v.findViewById(R.id.I4));
        grid.add((ImageButton) v.findViewById(R.id.I5));
        grid.add((ImageButton) v.findViewById(R.id.I6));
        grid.add((ImageButton) v.findViewById(R.id.I7));
        grid.add((ImageButton) v.findViewById(R.id.I8));
        grid.add((ImageButton) v.findViewById(R.id.I9));
        grid.add((ImageButton) v.findViewById(R.id.I10));

        grid.add((ImageButton) v.findViewById(R.id.J1));
        grid.add((ImageButton) v.findViewById(R.id.J2));
        grid.add((ImageButton) v.findViewById(R.id.J3));
        grid.add((ImageButton) v.findViewById(R.id.J4));
        grid.add((ImageButton) v.findViewById(R.id.J5));
        grid.add((ImageButton) v.findViewById(R.id.J6));
        grid.add((ImageButton) v.findViewById(R.id.J7));
        grid.add((ImageButton) v.findViewById(R.id.J8));
        grid.add((ImageButton) v.findViewById(R.id.J9));
        grid.add((ImageButton) v.findViewById(R.id.J10));

        //add listeners to grid
        for (ImageView view : grid) {
            view.setOnClickListener(listener);
        }

        fireButton = (Button) v.findViewById(R.id.FireButton);
        fireButton.setOnClickListener(listener);
        fireButton.setEnabled(false);

        forfeitButton = (Button) v.findViewById(R.id.ForfeitButton);
        forfeitButton.setOnClickListener(listener);

        switchButton = (Button) v.findViewById(R.id.SwitchButton);
        switchButton.setOnClickListener(listener);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save State Here
        saveStateToArguments();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Save State Here
        saveStateToArguments();
    }

    ////////////////////
    // Don’t Touch !!
    ////////////////////

    private void saveStateToArguments() {
        if (getView() != null)
            savedState = saveState();
        if (savedState != null) {
            Bundle b = getArguments();

            b.putBundle("saved", savedState);
        }
    }

    ////////////////////
    // Don’t Touch !!
    ////////////////////

    private boolean restoreStateFromArguments() {
        Bundle b = getArguments();
        savedState = b.getBundle("saved");
        if (savedState != null) {
            restoreState();
            return true;
        }
        return false;
    }

    /////////////////////////////////
    // Restore Instance State Here
    /////////////////////////////////

    private void restoreState() {
        if (savedState != null) {

            ArrayList<String> tags = savedState.getStringArrayList("GTAGS");

            fireButton.setEnabled(savedState.getBoolean("fireOn"));

            int i = 0;

            SalvoActivity activity = (SalvoActivity) getActivity();

            for (ImageView view : grid) {

                view.setTag(tags.get(i));
                view.setBackgroundResource(activity.getImg(tags.get(i)));

                i++;
            }
        }
    }

    //////////////////////////////
    // Save Instance State Here
    //////////////////////////////

    private Bundle saveState() {
        Bundle state = new Bundle();

        ArrayList<String> tags = new ArrayList<>();

        for (ImageView view : grid) {
            tags.add((String) view.getTag());
        }

        state.putStringArrayList("GTAGS", tags);
        state.putBoolean("fireOn", fireButton.isEnabled());

        return state;
    }

    public void ping(String msg) {
        if (msg.charAt(0) == 'M') {
            //if its a miss
            grid.get(grid.indexOf(tempTarget)).setTag("miss");
            grid.get(grid.indexOf(tempTarget)).setBackgroundResource(R.drawable.miss);
        } else {
            //if its a hit
            grid.get(grid.indexOf(tempTarget)).setTag("hit");
            grid.get(grid.indexOf(tempTarget)).setBackgroundResource(R.drawable.hit);
            if (msg.length() > 1) {
                switch (msg.charAt(1)) {
                    case 'a':
                        variableToast("Aircraft Carrier Sunk!", 2000);
                        break;
                    case 'b':
                        variableToast("Battleship Sunk!", 2000);
                        break;
                    case 'd':
                        variableToast("Destroyer Sunk!", 2000);
                        break;
                    case 'p':
                        variableToast("Patrol Boat Sunk!", 2000);
                        break;
                    case 's':
                        variableToast("Submarine Sunk!", 2000);
                        break;
                }
            }

        }
    }

    //Allows for shorter Length toasts
    public void variableToast(String message, int duration) {
        final Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, duration);
    }

    private class BtnListener implements View.OnClickListener {
        @Override
        // On-click event handler -- method definition
        public void onClick(View view) {
            SalvoActivity activity = (SalvoActivity) getActivity();

            switch (view.getId()) {

                case R.id.FireButton:
                    if (target == null) {
                        variableToast("NO TARGET", 500);
                    } else {
                        if (activity != null) {
                            tempTarget = target;
                            activity.sendMsg("F" + grid.indexOf(target));
                            target = null;

                        }
                    }
                    break;
                case R.id.SwitchButton:
                    switchToBottom();
                    break;
                case R.id.ForfeitButton:
                    //end the game
                    activity.concede();
                    break;
                default:
                    if ("empty".equals(view.getTag())) {
                        if (target != null && target.getTag().equals("reticule")) {
                            //change image and tag of previous back to empty
                            target.setTag("empty");
                            target.setBackgroundResource(R.drawable.empty);
                        }
                        //change image and tag of target to reticule
                        target = view;
                        target.setTag("reticule");
                        target.setBackgroundResource(R.drawable.reticule);
                    } else {
                        variableToast("INVALID TARGET", 500);
                    }//if
            }//switch
        }//OnClick

        private void switchToBottom() {
            //switch it to the fleet or bottom screen
            SalvoActivity activity = (SalvoActivity) getActivity();
            if (activity != null) {
                GameWindowFragment frag = activity.getGameWindow();
                frag.switchWindow();
            }
        }

    }//BtnListener
}